# -*- coding: utf-8 -*-

from selenium.webdriver.common.keys import Keys
from scrapy.selector import Selector
from selenium import webdriver
from time import sleep

import datetime
import random 
import scrapy
import json

class GhostRequest(object):

	ONE_WAY_SELECTOR = '#flt-modaldialog > div > div.gws-flights-dialog__calendar-header.gws-flights__flex-box > div.gws-flights__flex-filler.gws-flights-dialog__calendar-reset-container > div > div > dropdown-menu > div > div.mSPnZKpnf91__menu.mSPnZKpnf91__cover-button.mSPnZKpnf91__open > menu-item:nth-child(3)'
	INPUT_HANDLE_DISPLAY_ORIGIN = '#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > div.gws-flights__flex-column.gws-flights__active-tab.gws-flights__home-page > div.gws-flights__center-content.gws-flights__scrollbar-padding > div.gws-flights__form.gws-flights__homepage-form.gws-flights-form__search-form > div.gws-flights-form__form-content > div.gws-flights__flex-box.gws-flights__align-center > div.flt-input.gws-flights-form__input-container.gws-flights__flex-box.gws-flights-form__airport-input.gws-flights-form__swapper-right > div.gws-flights-form__location-text.gws-flights__flex-filler.gws-flights__ellipsize.gws-flights-form__input-target'
	INPUT_HANDLE_DISPLAY_DESTINATION = '#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > div.gws-flights__flex-column.gws-flights__active-tab.gws-flights__home-page > div.gws-flights__center-content.gws-flights__scrollbar-padding > div.gws-flights__form.gws-flights__homepage-form.gws-flights-form__search-form > div.gws-flights-form__form-content > div.gws-flights__flex-box.gws-flights__align-center > div.flt-input.gws-flights-form__input-container.gws-flights__flex-box.gws-flights-form__airport-input.gws-flights-form__empty.gws-flights-form__swapper-left > div.gws-flights-form__location-text.gws-flights__flex-filler.gws-flights__ellipsize.gws-flights-form__input-target.gws-flights-form__empty'
	INPUT_HANDLE_DISPLAY_DATE = '#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > div.gws-flights__flex-column.gws-flights__active-tab.gws-flights__home-page > div.gws-flights__center-content.gws-flights__scrollbar-padding > div.gws-flights__form.gws-flights__homepage-form.gws-flights-form__search-form > div.gws-flights-form__form-content > div.gws-flights__flex-box.gws-flights__align-center > div.gws-flights-form__input-container.gws-flights__flex-box.gws-flights__flex-filler.gws-flights-form__calendar-input > div.flt-input.gws-flights__flex-box.gws-flights__flex-filler.gws-flights-form__departure-input.gws-flights-form__round-trip'
	INPUT_HANDLE_DISPLAY_TYPE_TRIP = '#flt-modaldialog > div > div.gws-flights-dialog__calendar-header.gws-flights__flex-box > div.gws-flights__flex-filler.gws-flights-dialog__calendar-reset-container > div > div > dropdown-menu > div > div.gws-flights__white-focus.gws-flights-form__menu-button'
	DONE_BUTTON_DATE_SELECTOR  = '#flt-modaldialog > div > div.gws-flights-dialog__calendar-footer > g-raised-button > div'
	INPUT_SELECTOR = '#sb_ifc50 > input[type="text"]'
	INPUT_DATE_SELECTOR = '.qCutsdOnIDY__date-input'

	"""
		@params {INIT} GhostRequest
		@description init the driver
	"""	
	def __init__(self):
		self.driver = webdriver.Firefox()
		
	"""
		@params {String, Dictionnary, String} url, couple, date
		@description start the bot for automate the scraper without formating the url in case one day it will not be possible anymore 
		@return new object wich contains the url for the trip and the source page
	"""	
	def start(self, url, couple, date):

		self.driver.get(url)
		"""
			insert origin city in the form
		"""
		self.click_element(self.INPUT_HANDLE_DISPLAY_ORIGIN)
		self.send_keys_to_input(self.INPUT_SELECTOR, couple['codeOrigin'])
		"""
			insert destination city in the form
		"""
		self.click_element(self.INPUT_HANDLE_DISPLAY_DESTINATION)
		self.send_keys_to_input(self.INPUT_SELECTOR, couple['codeTo'])

		"""
			insert date in the form and select one way as trip type
		"""
		self.click_element(self.INPUT_HANDLE_DISPLAY_DATE)
		self.click_element(self.INPUT_HANDLE_DISPLAY_TYPE_TRIP)
		self.click_element(self.ONE_WAY_SELECTOR)

		self.send_keys_to_input(self.INPUT_DATE_SELECTOR, date)
		"""
			handle form
		"""
		self.click_element(self.DONE_BUTTON_DATE_SELECTOR)

	"""
		@params {String} selector
		@description click on the html element of the given css selector
	"""
	def click_element(self, selector):
		self.driver.find_element_by_css_selector(selector).click()
		sleep(1)

	"""
		@params {String, String} selector, key
		@description fill the form with the given key to the given input selector
	"""
	def send_keys_to_input(self, selector, key):
		element = self.driver.find_element_by_css_selector(selector)
		sleep(1)
		element.send_keys(key)
		sleep(1)
		element.send_keys(Keys.ENTER)
		sleep(1)

	"""
		@params {String} selector 
		@description press enter key
	"""
	def send_enter_key(self, selector):
		element = self.driver.find_element_by_css_selector(selector)
		sleep(1)
		element.send_keys(Keys.ENTER)
		sleep(1)

	"""
		@params {String} url
		@description return source page from given url with javascript execution
		@return {Response}
	"""
	def get_source_page_from_url(self, url):
		self.driver.get(url)
		sleep(1)
		try:
			sleep(5)
		except:
			sleep(5)

		return self.driver.page_source

# __ Spider helpers with some useful method __
class helpersSpider(object):

	"""
		@params {Array} listAgent
		@description return user-agent generator from list
	"""
	def user_agent_generator(self, listAgent):
		for k in listAgent:
			yield k

	"""
		@params {String, Int} strDate, nb
		@description add nb days to a strDate
		@return {String} date
	"""
	def add_day_to_str_date(self, strDate, nb):
		date = datetime.datetime.strptime(strDate, "%Y-%m-%d")
		modified_date = date + datetime.timedelta(days=nb)
		date = datetime.datetime.strftime(modified_date, "%Y-%m-%d")

		return date

	"""
		@params {String, String} strTime, strDate
		@description concat string date and string time
		@return {Datetime} datetime_object
	"""
	def get_date_time_object(self, strTime, strDate):
		datetime_object = datetime.datetime.strptime('{date} {time}'.format(date=strDate, time=strTime) , '%Y-%m-%d %H:%M %p')

		return datetime_object

	"""
		@params {String} time
		@description get minute format from hour format
		@return {Int} 
	"""
	def get_minute_format(self, time):
		splited_time = time.split()
		converted = int(splited_time[0].replace("h", "")) * 60

		return (converted + int(splited_time[1].replace("m", "")))

	"""
		@params {Dictionnary} obj
		@description return same object with failed state if obj contains none value for handle error
		@return {Dictionnary} 
	"""
	def update_state_if_none(self, obj):
		for k in obj:

			if obj[k] is None:
				obj["state"] = 0

		return obj


class GooglespiderSpider(scrapy.Spider):

	name = 'googleSpider'
	
	CITY_TO_SCRAPE = [ 'Johannesburg' , 'Cape Town' , 'Sydney' , 'Melbourne' , 'Jakarta', 'New Delhi' , 'Mumbai', 'Seoul' , 'Jeju' , 'Jeddah', 'Istanbul', 'Antalya', 'Tokyo', 'Osaka', 'Beijing', 'Shanghai', 'Guangzhou', 'Moscow', 'St. Petersburg', 'Hong Kong' ]
	GOOGLE_FLIGHT_URL_FORMAT = 'https://www.google.com/flights/?hl=en&gl=fr#flt={origin}.{destination}.{date};c:EUR;e:1;s:0*0;sd:1;t:f'
	GOOGLE_FLIGHT_URL = 'https://www.google.com/flights/?hl=en'
	NUMBER_OF_DAY_TO_SCRAPE = 22

	"""
		@description {INIT} googleSpider
	"""
	def __init__(self):

		#__ we get our couples __
		with open('./json/routes.json') as data_file:    
			self.list_airport = json.load(data_file)
		# __ we get our user agent list for the switcher to prevent ban __
		with open('./json/userAgent.json') as agent:
			self.jsonList = json.load(agent)
		
		self.help = helpersSpider()
		self.ghost = GhostRequest()

		self.userAgentList = self.help.user_agent_generator(self.jsonList)

	"""
		@description get the following user-agent from generator
	"""
	def user_agent_apply_next(self):
		try :
			self.selectedUserAgent = self.userAgentList.next()
		except:
			# __ endpoint of generator => go back ___
			self.userAgentList = self.help.user_agent_generator(self.jsonList)
			self.userAgentList.next()

	"""
		@params {String} uri
		@description request in a loop to prevent unloaded page and crash (return : source page)
		@return {Dictionnary} { "state" : <1 = OK 0 = Fail> , "source" : <html of the given url> }
	"""
	def request_loop_wth_anticrash(self,  uri):
		CELLULE_LOADED_SELECTOR = 'div.gws-flights-results__select-header.gws-flights__flex-filler'

		i = 0;
		celulle = []

		while not celulle:

			if i == 5:
				return { "state" : 0 , "source" : uri }

			page = self.ghost.get_source_page_from_url(uri)
			celulle = Selector(text=page).css(CELLULE_LOADED_SELECTOR) if Selector(text=page).css(CELLULE_LOADED_SELECTOR) is not None else Selector(text=page).css(CELLULE_LOADED_SELECTOR)
			i = i + 1
		
		return { "state" : 1 , "source" : page }

	"""
		@params {Dictionnary, Dictionnary} uri
		@description return structured data in dictionnary and add the success state and format the string datetime to datetime object
		@return {Dictionnary}
	"""
	def return_data_formated(self, couple, parsedData):

		return({
			"state" : 1,
			"origin" : couple["origin"],
			"dest" : couple["to"],
			"airportOrigin" : couple["codeOrigin"],
			"airportTo": couple["codeTo"],
			"time_dep" : self.help.get_date_time_object(parsedData["dep"], parsedData["date"]),
			"time_ari" : self.help.get_date_time_object(parsedData["ari"] , parsedData["date"]),
			"price" : parsedData["price"],
			"company" : parsedData["company"],
			"time_of_fly" : self.help.get_minute_format(parsedData["time_of_fly"])
		})

	"""
		@params {NodeDOM, Dictionnary, String} node, couple, date
		@description parse the source page by using css selector and extract the flight data
		@return {Dictionnary} { "state" : <1 = OK 0 = Fail> ...  }  -> object wich contains the structured data
	"""	
	def parse_source_page(self, node, couple, date):

		AIRLINE_SELECTOR = 'div.gws-flights__flex-box.gws-flights__align-center > span:nth-child(4) > span > span:nth-child(2)'
		DEPARTURE_TIME_SELECTOR = 'div.gws-flights-results__times > span:nth-child(1) > span > jsl > span:nth-child(1)'
		ARIVAL_TIME_SELECTOR = 'div.gws-flights-results__times > span:nth-child(2) > span > jsl > span:nth-child(1)'
		PRICE_SELECTOR = 'div.gws-flights-results__price > jsl:nth-child(1) > jsl'
		TIME_OF_FLY_SELECTOR = 'div.gws-flights-results__duration'

		try :
			
			price = node.css('{price_selector}::text'.format(price_selector=PRICE_SELECTOR)).extract_first()[1:]
			time_of_fly = node.css('{time_selector}::text'.format(time_selector=TIME_OF_FLY_SELECTOR)).extract_first()
			departure_time = node.css('{departure_time}::text'.format(departure_time=DEPARTURE_TIME_SELECTOR)).extract_first()
			arival_time = node.css('{arival_time}::text'.format(arival_time=ARIVAL_TIME_SELECTOR)).extract_first()
			airline = node.css('{airline}::text'.format(airline=AIRLINE_SELECTOR)).extract_first()
	
			returned = self.return_data_formated(couple, { 
					"price" : price.replace(",",""), 
					"company" : airline, 
					"dep" : departure_time, 
					"ari" : arival_time, 
					"time_of_fly" : time_of_fly , 
					"date" : date 
				} 
			)

			return self.help.update_state_if_none(returned)

		except Exception as e:
			print(e)
			return ( { "state" : 0 } )

	"""
		@description fake start request -> prevent referer
	"""
	def start_requests(self):

		yield scrapy.Request(self.GOOGLE_FLIGHT_URL, callback=self.parse_process)


	"""
		@description start parser, PROCESS => format url -> send url to sellenium -> get page source -> extract data -> yield it to pipeline.
	"""
	def parse_process(self, response):
		date = self.help.add_day_to_str_date(datetime.datetime.now().strftime("%Y-%m-%d"), 1)
		dailyIndex = 0
		
		while dailyIndex < self.NUMBER_OF_DAY_TO_SCRAPE:
			for couple in self.list_airport:

				if couple["origin"] in self.CITY_TO_SCRAPE or couple["to"] in self.CITY_TO_SCRAPE:
					swap_origin_dest_index = 0

					while swap_origin_dest_index < 2:

						page = self.request_loop_wth_anticrash(self.GOOGLE_FLIGHT_URL_FORMAT.format(origin=couple['codeOrigin'], destination=couple['codeTo'], date=date))
						items = self.parse_box(page['source'], couple, date) if page['state'] is 1 else None

						if items is not None:
							# __ loop over generator __
							for it in items:
								yield it

						couple["codeOrigin"], couple["codeTo"] = couple["codeTo"], couple["codeOrigin"]
						couple["origin"], couple["to"] = couple["to"], couple["origin"]
						swap_origin_dest_index = swap_origin_dest_index + 1
						sleep(random.uniform(2, 4))

			dailyIndex += 1
			self.user_agent_apply_next()
			date = self.help.add_day_to_str_date(date, 2)

	"""
		@description loop over container and send the current node to the parse
		@params {String, Dictionnary, String} page, couple, date
		@return {Dictionnary}
	"""
	def parse_box(self, page, couple, date):
		BOX_DATA_SELECTOR = 'div.gws-flights-results__select-header.gws-flights__flex-filler'



		for div in Selector(text=page).css(BOX_DATA_SELECTOR):
			returned = self.parse_source_page(div, couple, date)

			if returned['state'] is 1:
				yield returned
