import datetime
import MySQLdb
import scrapy
import random 
import json 

class GoogleFlightV2Pipeline(object):
	
	def __init__(self):

		with open('./json/db.json') as log:
			logs = json.load(log)
		
		user = logs["username"]
		passwd = logs["password"]
		host = logs["host"]
		db = logs["db"]

		self.dbb = MySQLdb.connect(host, user, passwd, db, charset="utf8", use_unicode=True ) 
		self.cnx = self.dbb.cursor()

	def close_spider(self, spider):
		# __ all close __ #
		self.dbb.close()
		spider.ghost.driver.quit()

	def process_item(self, item, spider):
		now = today = datetime.datetime.now()
		print(item)
		try: 

			self.cnx.execute(""" INSERT INTO google_flights_prices VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", (item["origin"], item["dest"], item["airportOrigin"], item["airportTo"], item["time_dep"], item["time_ari"], item["company"], item["time_of_fly"], item["price"], now))
			self.dbb.commit()

			print("Success")

		except Exception as e :

			print("Failes an error occured:  " , e)

		return item